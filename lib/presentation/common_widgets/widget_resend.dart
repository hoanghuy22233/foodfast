import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/style/style.dart';
import 'package:foodsmy/presentation/common_widgets/widget_link.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

class WidgetResend extends StatelessWidget {
  final Function onTap;
  final int time;
  final bool isValid;

  const WidgetResend({Key key, this.onTap, this.time, this.isValid})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String tmpTime = '';
    if (time < 10) {
      tmpTime = '00:0${time}';
    } else {
      tmpTime = '00:${time}';
    }
    return Container(
      child: Wrap(
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        runAlignment: WrapAlignment.center,
        children: [
          WidgetLink(
            text: !isValid
                ? '${AppLocalizations.of(context).translate('register_verify.resend')} ($tmpTime)'
                : '${AppLocalizations.of(context).translate('register_verify.resend')}',
            style: !isValid
                ? AppStyle.APP_MEDIUM_BOLD.copyWith(
                    color: AppColor.GREY,
                    decoration: TextDecoration.underline,
                  )
                : AppStyle.APP_MEDIUM_BOLD.copyWith(
                    color: AppColor.GREY,
                    decoration: TextDecoration.underline,
                  ),
            onTap: !isValid ? null : onTap,
          )
        ],
      ),
    );
  }
}
