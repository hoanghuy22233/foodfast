import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/style/style.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_logo.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:foodsmy/presentation/screen/register/widget_bottom_login.dart';
import 'package:foodsmy/presentation/screen/register/widget_register_appbar.dart';
import 'package:foodsmy/presentation/screen/register/widget_register_form.dart';

import 'bloc/register_bloc.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => RegisterBloc(userRepository: userRepository),
          child: Container(
              color: AppColor.PRIMARY_BACKGROUND,
              child: Column(
                children: [
                  _buildAppbar(),
                  SizedBox(height: MediaQuery.of(context).size.height/6,),
                  _buildBackground(),
                  _buildRegisterForm(),
                ],
              )),
        ),
      ),
    );
  }

  _buildBackground(){
    return Column(
      children: [
        Text('Bánh tráng trộn'.toUpperCase(), style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
        SizedBox(height: 20,),
        Text('50B - Lương Khánh Thiện', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
      ],
    );
  }
  _buildAppbar() => WidgetRegisterAppbar();
  _buildRegisterForm() => WidgetRegisterForm();
}
