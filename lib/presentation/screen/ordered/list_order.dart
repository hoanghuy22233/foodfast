import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/model/entity/order.dart';

class ListOrder extends StatefulWidget {
  final int id;
  ListOrder({Key key, this.id}) : super(key: key);

  @override
  _ListOrderState createState() => _ListOrderState();
}

class _ListOrderState extends State<ListOrder> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child: Container(
          decoration: BoxDecoration(
            color: AppColor.PRIMARY_COLOR,
            borderRadius: BorderRadius.circular(10.0),
          ),
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Text(
                    "Mã đơn hàng : ",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )),
                  Text(
                    itemOrder[widget.id].code,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Text(
                    "Bàn số : ",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )),
                  Text(
                    itemOrder[widget.id].table,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: Text(
                    "Tổng : ",
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )),
                  Text(
                    itemOrder[widget.id].total,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
