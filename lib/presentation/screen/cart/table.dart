import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodsmy/model/entity/categorys.dart';
import 'package:foodsmy/utils/color/hex_color.dart';

class TableScreen extends StatefulWidget {
  final int id;
  TableScreen({Key key, this.id}) : super(key: key);

  @override
  _TableScreenState createState() => _TableScreenState();
}

class _TableScreenState extends State<TableScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child: Container(
        height: MediaQuery.of(context).size.height*0.6,
          child: Column(
          children: [
            Image.asset(
              '${itemCategory[widget.id].image}',
              width: 35,
              height: 35,
              color: Color(
                  HexColor.getColorFromHex(itemCategory[widget.id].colorImage)),
            ),
            SizedBox(
              height: 1,
            ),
            Text(
              '${itemCategory[widget.id].name}',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold),
            ),
          ],
        )
      ),
    );
  }
}
