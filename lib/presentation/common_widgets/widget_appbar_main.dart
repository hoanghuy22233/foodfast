import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/style/style.dart';
import 'package:foodsmy/app/constants/value/value.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu.dart';

class WidgetAppBarMain extends StatelessWidget {
  final String title;
  final Function openDrawer;
  final Function actionBack;
  final Color color;
  final bool canSearch;
  final bool heroSearch;
  final String tag;

  WidgetAppBarMain({
    Key key,
    this.canSearch = true,
    this.color,
    this.heroSearch = false,
    this.actionBack,
    this.title,
    this.tag,
    this.openDrawer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color ?? Colors.white,
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      child: Stack(
        children: [
//          title != null
//              ? _buildTitle()
//              : WidgetLogo(
//                  widthPercent: 0.3,
//                ),
          title != null
              ? _buildTitle()
              : Container(
                  margin: const EdgeInsets.all(4.0),
                  height: AppValue.ACTION_BAR_HEIGHT * 1.25,
                ),
          Positioned.fill(
            child: Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(
                      width: 10,
                    ),
                    actionBack != null
                        ? Container(
                            width: AppValue.ACTION_BAR_HEIGHT * 0.8,
                            height: AppValue.ACTION_BAR_HEIGHT * 0.8,
                            child: WidgetAppbarMenu(
                              icon: Image.asset(
                                'assets/icons/ic_back.png',
                                color: Colors.deepOrange,
                              ),
                              onTap: actionBack,
                              backgroundColor: Colors.deepOrange,
                            ))
                        : Container(
                            width: AppValue.ACTION_BAR_HEIGHT,
                            height: AppValue.ACTION_BAR_HEIGHT,
                            child: WidgetAppbarMenu(
                              icon: Image.asset(
                                'assets/icons/ic_menu.png',
                                color: Colors.deepOrange,
                              ),
                              onTap: openDrawer,
                            ))
                  ],
                )),
          ),
          Positioned.fill(
            child: Align(
                alignment: Alignment.centerRight,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                        width: 30,
                        height: 30,
                        child: Image.asset(
                          'assets/icons/ic_cart.png',
                          height: 25,
                          color: Colors.deepOrange,
                        )),
                    canSearch
                        ? (heroSearch
                            ? Hero(
                                tag: "search",
                                child: Container(
                                    width: AppValue.ACTION_BAR_HEIGHT,
                                    height: AppValue.ACTION_BAR_HEIGHT,
                                    child: WidgetAppbarMenu(
                                      icon: Image.asset(
                                        'assets/images/img_search.png',
                                        color: Colors.deepOrange,
                                      ),
                                      onTap: () {
                                        //AppNavigator.navigateSearch();
                                      },
                                    )),
                              )
                            : Container(
                                width: AppValue.ACTION_BAR_HEIGHT,
                                height: AppValue.ACTION_BAR_HEIGHT,
                                child: WidgetAppbarMenu(
                                  icon: Image.asset(
                                    'assets/images/img_search.png',
                                    color: Colors.deepOrange,
                                  ),
                                  onTap: () {
                                    //AppNavigator.navigateSearch();
                                  },
                                )))
                        : SizedBox(),
                    const SizedBox(
                      width: 10,
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle() {
    return tag != null
        ? Hero(
            tag: tag,
            child: Container(
              margin: const EdgeInsets.all(4.0),
              height: AppValue.ACTION_BAR_HEIGHT * 1.25,
              child: Center(
                child: Text(
                  title.toUpperCase(),
                  style: AppStyle.DEFAULT_MEDIUM.copyWith(
                      color: AppColor.PRIMARY, fontWeight: FontWeight.bold),
                ),
              ),
            ))
        : Container(
            margin: const EdgeInsets.all(4.0),
            height: AppValue.ACTION_BAR_HEIGHT * 1.25,
            child: Center(
              child: Text(
                title.toUpperCase(),
                style: AppStyle.DEFAULT_MEDIUM.copyWith(
                    color: AppColor.PRIMARY, fontWeight: FontWeight.bold),
              ),
            ),
          );
  }
}
