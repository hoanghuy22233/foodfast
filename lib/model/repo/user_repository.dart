import 'dart:io';

import 'package:dio/dio.dart';
import 'package:foodsmy/model/api/request/change_app_request.dart';
import 'package:foodsmy/model/api/request/delete_post_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_reset_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_verify_request.dart';
import 'package:foodsmy/model/api/request/register_app_request.dart';
import 'package:foodsmy/model/api/request/update_birthday_request.dart';
import 'package:foodsmy/model/api/request/update_email_request.dart';
import 'package:foodsmy/model/api/request/update_name_request.dart';
import 'package:foodsmy/model/api/request/update_phone_request.dart';
import 'package:foodsmy/model/api/response/barrel_response.dart';
import 'package:foodsmy/model/api/response/forgot_password_reset_response.dart';
import 'package:foodsmy/model/api/response/forgot_password_response.dart';
import 'package:foodsmy/model/api/response/forgot_password_verify_response.dart';
import 'package:foodsmy/model/api/response/images_response.dart';
import 'package:foodsmy/model/api/response/location_response.dart';
import 'package:foodsmy/model/api/response/login_response.dart';
import 'package:foodsmy/model/api/response/news_detail_response.dart';
import 'package:foodsmy/model/api/response/post_diary_response.dart';
import 'package:foodsmy/model/api/response/profile_response.dart';
import 'package:foodsmy/model/api/response/update_avatar_response.dart';
import 'package:foodsmy/model/api/response/update_background_image_response.dart';
import 'package:foodsmy/model/api/response/update_birthday_response.dart';
import 'package:foodsmy/model/api/response/update_email_response.dart';
import 'package:foodsmy/model/api/response/update_gender_response.dart';
import 'package:foodsmy/model/api/response/update_location_response.dart';
import 'package:foodsmy/model/api/response/update_name_response.dart';
import 'package:foodsmy/model/api/response/update_phone_response.dart';
import 'package:foodsmy/model/api/rest_client.dart';
import 'package:meta/meta.dart';

import '../api/request/login_app_request.dart';

class UserRepository {
  final Dio dio;

  UserRepository({@required this.dio});

  Future<LoginResponse> loginApp(
      {@required String username, @required String password}) async {
    final client = RestClient(dio);
    return client
        .loginApp(LoginAppRequest(emailOrPhone: username, password: password));
  }

  Future<LoginResponse> registerApp(
      {@required String username,
      @required String password,
      @required String confirmPassword}) async {
    final client = RestClient(dio);
    return client.registerApp(RegisterAppRequest(
        emailOrPhone: username,
        password: password,
        confirmPassword: confirmPassword));
  }

  Future<LoginResponse> changePassWord(
      {@required String oldPass,
      @required String password,
      @required String confirmPassword}) async {
    final client = RestClient(dio);
    return client.changePassWord(ChangeAppRequest(
        oldPass: oldPass,
        password: password,
        confirmPassword: confirmPassword));
  }

  Future<ForgotPasswordResponse> forgotPassword(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.forgotPassword(ForgotPasswordRequest(emailOrPhone: username));
  }

  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      {@required String username, @required String otpCode}) async {
    final client = RestClient(dio);
    return client.forgotPasswordVerify(
        ForgotPasswordVerifyRequest(emailOrPhone: username, otpCode: otpCode));
  }

  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      {@required String username,
      @required String otpCode,
      @required String password,
      @required String confirmPassword}) async {
    final client = RestClient(dio);
    return client.forgotPasswordReset(ForgotPasswordResetRequest(
        emailOrPhone: username,
        otpCode: otpCode,
        password: password,
        confirmPassword: confirmPassword));
  }

  Future<NewsResponse> getNew() async {
    final client = RestClient(dio);
    return client.getNews();
  }

  Future<ImagesResponse> getImages() async {
    final client = RestClient(dio);
    return client.getImages();
  }

  Future<ProfileResponse> getProfile() async {
    final client = RestClient(dio);
    return client.getProfile();
  }

  Future<NewsDetailResponse> getNewsDetail({
    @required int newsId,
  }) async {
    final client = RestClient(dio);
    return client.getNewsDetail(newsId);
  }

  Future<NewsResponse> removePost({@required int id}) async {
    final client = RestClient(dio);
    return client.removePost(DeletePostRequest(id));
  }

  Future<LocationResponse> getPosition() async {
    final client = RestClient(dio);
    return client.getPosition();
  }

  //info
  Future<UpdateNameResponse> updateName({@required String name}) async {
    final client = RestClient(dio);
    return client.updateName(UpdateNameRequest(name: name));
  }

  Future<UpdatePhoneResponse> updatePhone({@required String phone}) async {
    final client = RestClient(dio);
    return client.updatePhone(UpdatePhoneRequest(phoneNumber: phone));
  }

  Future<UpdateEmailResponse> updateEmail({@required String email}) async {
    final client = RestClient(dio);
    return client.updateEmail(UpdateEmailRequest(email: email));
  }

  Future<UpdateBirthdayResponse> updateBirthDay(
      {@required String birthDay}) async {
    final client = RestClient(dio);
    return client.updateBirthday(UpdateBirthdayRequest(dateOfBirth: birthDay));
  }

  Future<UpdateAvatarResponse> updateAvatar({@required File avatarFile}) async {
    final client = RestClient(dio);
    return client.updateAvatar(avatarFile);
  }

  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      {@required File backgroundImageFile}) async {
    final client = RestClient(dio);
    return client.updateBackgroundImage(backgroundImageFile);
  }

  Future<UpdateGenderResponse> genDer({@required int superId}) async {
    final client = RestClient(dio);
    return client.genDer(superId);
  }

  Future<UpdateLocationResponse> postLocation({
    @required String lat,
    @required String long,
  }) async {
    final client = RestClient(dio);
    return client.updateLocation(lat, long);
  }

  Future<PostDiaryResponse> postDiary({
    @required String title,
    @required String content,
    File images,
    int type,
    int work,
    double lat,
    double long,
  }) async {
    final client = RestClient(dio);
    return client.postDiary(images, title, content, lat, long, type, work);
  }

  Future<PostDiaryResponse> postDiaryNoImage({
    @required String title,
    @required String content,
    int type,
    int work,
    double lat,
    double long,
  }) async {
    final client = RestClient(dio);
    return client.postDiaryNoImage(title, content, lat, long, type, work);
  }

  Future<PostDiaryResponse> editDiary(
      {@required int id,
      @required String title,
      @required String content,
      File images,
      int type,
      int work,
      double lat,
      double long}) async {
    final client = RestClient(dio);
    return client.editDiary(id, images, title, content, lat, long, type, work);
  }

  Future<PostDiaryResponse> editDiaryNoImage(
      {@required int id,
      @required String title,
      @required String content,
      int type,
      int work,
      double lat,
      double long}) async {
    final client = RestClient(dio);
    return client.editDiaryNoImage(id, title, content, lat, long, type, work);
  }
}
