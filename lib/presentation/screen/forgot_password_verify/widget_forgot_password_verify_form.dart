import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_button.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:foodsmy/utils/snackbar/get_snack_bar_utils.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'bloc/bloc.dart';

class WidgetForgotPasswordVerifyForm extends StatefulWidget {
  final String username;

  const WidgetForgotPasswordVerifyForm({Key key, @required this.username})
      : super(key: key);

  @override
  _WidgetForgotPasswordVerifyFormState createState() =>
      _WidgetForgotPasswordVerifyFormState();
}

class _WidgetForgotPasswordVerifyFormState
    extends State<WidgetForgotPasswordVerifyForm> {
  ForgotPasswordVerifyBloc _registerVerifyBloc;

  TextEditingController _otpCodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _registerVerifyBloc = BlocProvider.of<ForgotPasswordVerifyBloc>(context);
    _otpCodeController.addListener(_onOtpCodeChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateForgotPasswordReset(
              username: widget.username, otpCode: _otpCodeController.text);
        }
        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
        }
      },
      child: BlocBuilder<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildCodeField(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildButtonVerify(state),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool get isPopulated => _otpCodeController.text.isNotEmpty;

  bool isForgotPasswordButtonEnabled() {
    return _registerVerifyBloc.state.isFormValid &&
        isPopulated &&
        !_registerVerifyBloc.state.isSubmitting;
  }

  _buildButtonVerify(ForgotPasswordVerifyState state) {
    return WidgetLoginButton(
      onTap: () {
        // if (isForgotPasswordButtonEnabled()) {
        //   _registerVerifyBloc.add(ForgotPasswordVerifySubmitted(
        //     username: widget.username,
        //     otpCode: _otpCodeController.text,
        //   ));
        // }

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ForgotPasswordResetScreen()),
        );
      },
      isEnable: isForgotPasswordButtonEnabled(),
      text: "Xác nhận",
    );
  }

  Widget _buildCodeField() {
    return PinCodeTextField(
      onChanged: (changed) {},
      length: 6,
      obsecureText: false,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderWidth: 1,
        inactiveColor: AppColor.PRIMARY_COLOR,
        inactiveFillColor: AppColor.GREY_LIGHTER_3,
        selectedColor: AppColor.THIRD_COLOR,
        selectedFillColor: AppColor.THIRD_COLOR,
        activeColor: AppColor.GREY_LIGHTER_3,
        activeFillColor: AppColor.GREY_LIGHTER_3,
      ),
      animationDuration: Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      textInputType: TextInputType.phone,
      controller: _otpCodeController,
      onCompleted: (completed) {
        print("Completed: $completed");
      },
      beforeTextPaste: (text) {
        return true;
      },
    );
  }

  void _onOtpCodeChange() {
    _registerVerifyBloc.add(OtpCodeChanged(otpCode: _otpCodeController.text));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
