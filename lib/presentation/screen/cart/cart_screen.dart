import 'package:flutter/material.dart';
import 'package:foodsmy/model/entity_offline/drinks.dart';
import 'package:foodsmy/model/entity_offline/favorite.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/presentation/common_widgets/widget_button.dart';
import 'package:foodsmy/presentation/common_widgets/widget_cart_appbar_menu.dart';
import 'package:foodsmy/presentation/screen/choose_table/choose_table.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';
import 'package:foodsmy/model/entity_offline/cart.dart';
import 'package:foodsmy/presentation/screen/home_page/category.dart';
import 'package:foodsmy/presentation/screen/cart/table.dart';
import 'package:foodsmy/model/entity/categorys.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {

  double total=0;
  String idTable = '';
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    for(int i=0; i< cartItem.length; i++){
      total = total + (cartItem[i].name[cartItem[i].id]).price*cartItem[i].quantity;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _appbar(context),
            SizedBox(
              height: 45,
            ),
            _buildFood(context),
          ],
        ),
      ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Text('Tổng: ${AppValue.APP_MONEY_FORMAT.format(total)}', style: AppStyle.DEFAULT_SMALL_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
              ),
              Expanded(
                flex: 1,
                child:  WidgetButton(
                  onTap: () {
                  },
                  isEnable: true,
                  text: AppLocalizations.of(context).translate('cart.order'),
                ),
              )
            ],
          ),
        )
    );
  }

  Widget _appbar(BuildContext context){
    return Container(
      height: MediaQuery.of(context).size.height*0.15,
      decoration: BoxDecoration(
        color: AppColor.PRIMARY_COLOR
      ),
      padding: EdgeInsets.only(top: 10),
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 1,
            height: MediaQuery.of(context).size.width * 0.15,
          ),
          Positioned.fill(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                WidgetAppbarMenuBack()
              ],
            ),
          ),
          Positioned.fill(
            bottom: -40,
              child: FractionallySizedBox(
                widthFactor: .4,
                child: Padding(
                  padding: EdgeInsets.only(top: 30),
                  child: Align(
                      alignment: Alignment.center,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.network('https://picsum.photos/250?image=9'),
                      )
                  ),
                ),
              ))
        ],
      ),
    );
  }

  Widget _buildFood(BuildContext context){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          Text("Bánh tráng trộn", style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.BLACK),),
          SizedBox(height: 10,),
          Text("50B - Lương Khánh Thiện", style: AppStyle.DEFAULT_SMALL.copyWith(color: AppColor.BLACK),),
          SizedBox(height: 10,),
          Container(
            height: MediaQuery.of(context).size.height * 0.55,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.lightBlue
              )
            ),
            child: Column(
              children: [
                SizedBox(height: 10,),
                Text("Mã đơn đặt: 666888", style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.BLACK),),
                Container(
                  height: MediaQuery.of(context).size.height * 0.55 - 30,
                  child: ListView.builder(
                    itemCount: cartItem.length,
                    itemBuilder: (context, index) {
                      return
                        ListTile(
                          title: _buildItem(cartItem[index],),
                      );
                    },
                  ),
                ),
              ],
            )
          ),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Row(
              children: [
                Expanded(
                  flex: 2,
                  child: Text("Bàn: $idTable", style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.BLACK),)),
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: (){
                      setState(() {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => _buildAboutDialog(context),
                        );
                      });
                    },
                    child: Text(AppLocalizations.of(context).translate('cart.choose_table'), style: AppStyle.DEFAULT_MEDIUM.copyWith(color: Colors.redAccent, decoration: TextDecoration.underline), ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(CartItem x){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          flex: 6,
          child: Text(x.name[x.id].name, style: AppStyle.DEFAULT_MEDIUM,),
        ),
        Expanded(
          flex: 1,
          child: Text('x${x.quantity.toString()}', style: AppStyle.DEFAULT_MEDIUM),
        ),
        Expanded(
          flex: 3,
          child: Text(AppValue.APP_MONEY_FORMAT.format((x.name[x.id].price * x.quantity)), style: AppStyle.DEFAULT_SMALL.copyWith(color: AppColor.PRIMARY_COLOR)),
        )
      ],
    );
  }

  Widget _buildAboutDialog(BuildContext context) {
    return new AlertDialog(
        title: const Text('Chọn bàn'),
        content: new GridView.builder(
            shrinkWrap: true,
            //padding: EdgeInsets.only(top: 10, left: 5, right: 5),
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 0),
                child: GestureDetector(
                  onTap: (){
                    print('Số bàn: ${index +1}');
                    setState(() {
                      idTable = (index +1).toString();
                    });
                  },
                  child: TableScreen(
                    id: index,
                  ),
                ),
              );
            },
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 5,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
                childAspectRatio: 0.65),
            physics: NeverScrollableScrollPhysics(),
            itemCount: itemCategory.length),
        actions: <Widget>[
          new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Okay!'),
          ),
      ],
    );
  }
}
