import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/app/constants/string/validator.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_button.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_input.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/screen/login/bloc/login_event.dart';
import 'package:foodsmy/presentation/screen/login/bloc/login_state.dart';
import 'package:foodsmy/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/login_bloc.dart';

class WidgetLoginForm extends StatefulWidget {
  @override
  _WidgetLoginFormState createState() => _WidgetLoginFormState();
}

class _WidgetLoginFormState extends State<WidgetLoginForm> {
  LoginBloc _loginBloc;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool obscurePassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    // _emailController.text = 'vietsuperman97@gmail.com';
    // _passwordController.text = '12345678';
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          GetSnackBarUtils.createSuccess(message: state.message);
          //AppNavigator.navigateNavigation();
          print("Đăng nhập thành công");
        }
        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
        return
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(right: 20, bottom: 10),
                      child: GestureDetector(
                        onTap: () {
                          AppNavigator.navigateForgotPassword();
                        },
                        child: Text(
                          "Quên mật khẩu ?",
                          textAlign: TextAlign.right,
                          style: TextStyle(
                              color: AppColor.PRIMARY_COLOR,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline),
                        ),
                      )),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildButtonLogin(state),
                  WidgetSpacer(
                    height: 20,
                  ),

                ],
              ),
            ),
          );
      }),
    );
  }

  _buildButtonLogin(LoginState state) {
    return WidgetLoginButton(
      onTap: () {
        // if (isRegisterButtonEnabled()) {
        //   _loginBloc.add(LoginSubmitUsernamePasswordEvent(
        //     email: _emailController.text.trim(),
        //     password: _passwordController.text.trim(),
        //   ));
        //   FocusScope.of(context).unfocus();
        // }

        AppNavigator.navigateNavigation();
      },
      isEnable: isRegisterButtonEnabled(),
      text: "Đăng nhập",
    );
  }

  bool isRegisterButtonEnabled() {
    return _loginBloc.state.isFormValid &&
        isPopulated &&
        !_loginBloc.state.isSubmitting;
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      autovalidate: autoValidate,
      inputController: _passwordController,
      onChanged: (value) {
        _loginBloc.add(LoginPasswordChanged(password: value));
      },
      validator: AppValidation.validatePassword("Vui lòng nhập mật khẩu"),
      hint: "Mật khẩu *",
      obscureText: obscurePassword,
      endIcon: IconButton(
        icon: Icon(
          obscurePassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePassword = !obscurePassword;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: AppColor.PRIMARY_COLOR,
      ),
    );
  }

  _buildTextFieldUsername() {
    return WidgetLoginInput(
      inputType: TextInputType.emailAddress,
      autovalidate: autoValidate,
      inputController: _emailController,
      leadIcon: new Icon(
        Icons.person,
        color: AppColor.PRIMARY_COLOR,
      ),
      onChanged: (value) {
        _loginBloc.add(LoginUsernameChanged(email: value));
      },
      validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
      hint: "Tài khoản *",
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
