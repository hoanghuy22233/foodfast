import 'package:foodsmy/model/api/response/base_response.dart';
import 'package:foodsmy/model/entity/position.dart';
import 'package:json_annotation/json_annotation.dart';

part 'location_response.g.dart';

@JsonSerializable()
class LocationResponse extends BaseResponse {
  List<Positions> data;

  LocationResponse(this.data);

  factory LocationResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LocationResponseToJson(this);
}
