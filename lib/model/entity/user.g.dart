// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['name'] as String,
    json['email'] as String,
    json['gender'] as int,
    json['phone_number'] as String,
    json['date_of_birth'] as String,
    json['type'] as int,
    json['avatar'] as String,
    json['background_image'] as String,
    json['status'] as int,
    (json['lat'] as num)?.toDouble(),
    (json['long'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'gender': instance.gender,
      'phone_number': instance.phoneNumber,
      'date_of_birth': instance.dateOfBirth,
      'type': instance.type,
      'avatar': instance.avatar,
      'background_image': instance.backgroundImage,
      'status': instance.status,
      'lat': instance.lat,
      'long': instance.long,
    };
