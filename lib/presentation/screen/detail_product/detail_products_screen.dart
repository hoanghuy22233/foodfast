import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:foodsmy/model/entity_offline/drinks.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/presentation/common_widgets/widget_button.dart';
import 'package:foodsmy/presentation/common_widgets/widget_cart_appbar_menu.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_button.dart';
import 'package:foodsmy/presentation/screen/all_product/poroduct_grid_item_no_check.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/presentation/screen/detail_product/widget_number_input.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

class DetailProductsScreen extends StatefulWidget {
  final int id;
  final List product;

  DetailProductsScreen(this.id, this.product);

  @override
  _DetailProductsScreenState createState() => _DetailProductsScreenState();
}

class _DetailProductsScreenState extends State<DetailProductsScreen> {

  final TextEditingController _numberController = TextEditingController();
  bool autoValidate = false;
  int quantity = 0;

  @override
  void initState() {
    _numberController.text = quantity.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _appbar(),
            _buildDetail(context),
          ],
        ),
      ),
      bottomNavigationBar: Row(
        children: [
          Expanded(
            flex: 1,
            child: WidgetButton(
              onTap: () {
                // if (!isRegisterButtonEnabled()) {
                //   _registerBloc.add(RegisterSubmitted(
                //       username: _usernameController.text,
                //       password: _passwordController.text,
                //       confirmPassword: _confirmPasswordController.text,
                //   ));
                //   FocusScope.of(context).unfocus();
                // }

              },
              isEnable: true,
              text: AppLocalizations.of(context).translate('product_detail.add_cart'),
            ),
          ),
          Expanded(
            flex: 1,
            child: WidgetButton(
              onTap: () {
                // if (!isRegisterButtonEnabled()) {
                //   _registerBloc.add(RegisterSubmitted(
                //       username: _usernameController.text,
                //       password: _passwordController.text,
                //       confirmPassword: _confirmPasswordController.text,
                //   ));
                //   FocusScope.of(context).unfocus();
                // }
                AppNavigator.navigateCart();
              },
              isEnable: true,
              text: AppLocalizations.of(context).translate('product_detail.order_now'),
            ),
          )
        ],
      ),
    );
  }

  _appbar() => WidgetAppbar(
    title: "Chi tiết",
    left: [
      WidgetAppbarMenuBack()
    ],
  );

  Widget _buildDetail(BuildContext context){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      // height: MediaQuery.of(context).size.height*0.7,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Container(
              height: MediaQuery.of(context).size.height*0.4,
              child: Image.asset(widget.product[widget.id].image, fit: BoxFit.fill,),
            ),
          ),
          SizedBox(height: 10,),
          Center(
            child: Text(
              widget.product[widget.id].introduce,
              style: AppStyle.DEFAULT_VERY_SMALL.copyWith(color: AppColor.RED),
              maxLines: 2,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 20,),
          Row(
            children: [
              Expanded(
              flex: 3,
                  child: Text('Tên: ', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.green),
                  ),
              ),
              Expanded(
                flex: 7,
                child: Text(widget.product[widget.id].name, style: AppStyle.DEFAULT_MEDIUM.copyWith(), textAlign: TextAlign.justify,maxLines: 2,),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            children: [
              Expanded(
                flex: 3,
                child: Text('Thành phần: ', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.green),)
              ),
              Expanded(
                flex: 7,
                child: Text(widget.product[widget.id].details != null ? widget.product[widget.id].details : 'Đang cập nhật', style: AppStyle.DEFAULT_MEDIUM.copyWith(), textAlign: TextAlign.justify,maxLines: 2,),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Row(
            children: [
              Expanded(
              flex: 3,
                child: Text('Giá: ', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.green),),
              ),
              Expanded(
              flex: 7,
              child: Text(AppValue.APP_MONEY_FORMAT.format(widget.product[widget.id].price), style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.RED),
                ),
              ),
            ],
          ),
          SizedBox(height: 20,),
          Center(
            child: Container(
              height: 40,
              width: 130,
              decoration: BoxDecoration(
                color: AppColor.THIRD_COLOR,
                border: Border.all(),
                borderRadius: BorderRadius.circular(25)
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                      child: Icon(Icons.remove_circle_outline,
                          color: AppColor.PRIMARY_COLOR, size: 25.0),
                      onTap: () {

                        setState(() {
                          if(quantity > 0){
                            quantity = quantity - 1;
                            _numberController.text = quantity.toString();
                          }

                        });
                      }),
                  Container(
                    width: 60,
                    decoration: BoxDecoration(

                      borderRadius: BorderRadius.circular(10)
                    ),
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    child: TextFormField(
                      controller: _numberController,
                      onChanged: (change) {
                      },
                      style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.PRIMARY_COLOR),
                      maxLines: 1,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: InputDecoration.collapsed(

                      ),
                    ),
                  ),
                  InkWell(
                    child: Icon(Icons.add_circle_outline,
                        color: AppColor.PRIMARY_COLOR, size: 25.0),
                    onTap: () {

                      setState(() {
                        quantity = quantity + 1;
                        _numberController.text = quantity.toString();
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
