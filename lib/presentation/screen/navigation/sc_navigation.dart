import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/presentation/common_widgets/widget_fab_bottom_nav.dart';
import 'package:foodsmy/presentation/screen/all_product/all_products_screen.dart';
import 'package:foodsmy/presentation/screen/profile/prrofile_screen.dart';
import 'package:foodsmy/presentation/screen/favorite/favorite_screen.dart';
import 'package:foodsmy/presentation/screen/home_page/home_page.dart';
import 'package:foodsmy/presentation/screen/ordered/order_page.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String store = '/store';
  static const String like = '/like';
  static const String order = '/order';
  static const String account = '/account';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  bool appbar = true;
  bool allowClose = false;

  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
    // BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/ic_nav_home.png',
          text: "Trang chủ"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.store,
          tabItem: TabItem.store,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_bag.png',
          text: "Món ăn"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.like,
          tabItem: TabItem.like,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/img_heart_actived.png',
          text: "Yêu thích"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.order,
          tabItem: TabItem.order,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/img_address_book.png',
          text: "Doanh thu"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_user.png',
          text: "Tài khoản"),
    ]);
  }

  void goToPage({int page}) {
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      // appBar: appbar
      //     ? AppBar(
      //         backgroundColor: Colors.blue,
      //         actions: <IconButton>[
      //           IconButton(
      //             icon: Icon(Icons.search),
      //             onPressed: () {
      //               AppNavigator.navigateSearch();
      //             },
      //           ),
      //         ],
      //       )
      //     : null,
      bottomNavigationBar: WidgetFABBottomNav(
        backgroundColor: AppColor.STATUS_BAR,
        selectedIndex: _selectedIndex,
        onTabSelected: (index) async {
          goToPage(page: index);
        },
        items: _navMenus,
        selectedColor: Colors.deepOrange,
        color: AppColor.NAV_ITEM_COLOR,
      ),
      // drawer: AppDrawer(
      //   drawer: _drawerKey,
      //   action: goToPage,
      //   cendalar: goToPage,
      //   map: goToPage,
      //   account: goToPage,
      // ),
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (newPage) {
          setState(() {
            this._selectedIndex = newPage;
          });
        },
        children: [
          HomeScreen(),
          AllProductsScreen(
              // drawer: _drawerKey,
              ),
          FavoriteScreen(),
          OrderScreen(
              // drawer: _drawerKey,
              ),
          ProfileScreen(),
        ],
      ),
    );
  }
}
