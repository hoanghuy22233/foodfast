import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/utils/common/common_utils.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

class WidgetForgotPasswordVerifyUsername extends StatelessWidget {
  final String username;

  const WidgetForgotPasswordVerifyUsername({Key key, @required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
              AppLocalizations.of(context).translate('register_verify.message'),
              style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.PRIMARY_COLOR)
          ),
          WidgetSpacer(
            height: 10,
          ),
          Text('${AppCommonUtils.hideUserName(username)}',
              style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.PRIMARY_COLOR)
    ),
        ],
      ),
    );
  }
}
