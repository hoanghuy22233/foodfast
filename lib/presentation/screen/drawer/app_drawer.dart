// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:foodsmy/presentation/common_widgets/widget_circle_progress.dart';
// import 'package:foodsmy/presentation/router.dart';
// import 'package:foodsmy/utils/handler/http_handler.dart';
//
// class AppDrawer extends StatefulWidget {
//   final Function action;
//   final Function cendalar;
//   final Function account;
//   final Function map;
//   final GlobalKey<ScaffoldState> drawer;
//   AppDrawer({this.action, this.account, this.cendalar, this.map, this.drawer});
//
//   @override
//   _AppDrawerState createState() => _AppDrawerState();
// }
//
// class _AppDrawerState extends State<AppDrawer> {
//   @override
//   void initState() {
//     super.initState();
//     BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//         child: BlocListener<ProfileBloc, ProfileState>(
//       listener: (context, state) async {
//         if (state is ProfileNotLoaded) {
//           await HttpHandler.resolve(status: state.error);
//         }
//       },
//       child: BlocBuilder<ProfileBloc, ProfileState>(
//         builder: (context, state) {
//           if (state is ProfileLoaded) {
//             return Container(
//               color: Colors.white,
//               child: ListView(
//                 padding: EdgeInsets.zero,
//                 scrollDirection: Axis.vertical,
//                 children: <Widget>[
//                   _createHeader(state),
//                   Divider(
//                     color: Colors.white,
//                     thickness: 1.0,
//                   ), //0569 486 879
//                   _createDrawerItem(
//                       icon: AssetImage("assets/images/note.png"),
//                       text: 'Đăng bài',
//                       onTap: () => Navigator.pushNamed(
//                           context, BaseRouter.CREATE_DIARY)),
//                   _createDrawerItem(
//                     icon: AssetImage("assets/images/notepad.png"),
//                     text: 'Tạo ghi chú',
//                     onTap: () {
//                       widget.cendalar(page: 1);
//                       Navigator.pop(context);
//                     },
//                   ),
//                   _createDrawerItem(
//                     icon: AssetImage("assets/images/card_image.png"),
//                     text: 'Kho ảnh cá nhân',
//                     onTap: () {
//                       widget.action(page: 2);
//                       Navigator.pop(context);
//                     },
//                   ),
//                   _createDrawerItem(
//                     icon: AssetImage("assets/images/map.png"),
//                     text: 'Bản đồ',
//                     onTap: () {
//                       widget.map(page: 3);
//                       Navigator.pop(context);
//                     },
//                   ),
//                 ],
//               ),
//             );
//           } else if (state is ProfileLoading) {
//             return Center(
//               child: WidgetCircleProgress(),
//             );
//           } else if (state is ProfileNotLoaded) {
//             return Center(
//               child: Text('${state.error}'),
//             );
//           } else {
//             return Center(
//               child: Text('Unknown state'),
//             );
//           }
//         },
//       ),
//     ));
//   }
//
//   Widget _createHeader(ProfileLoaded state) {
//     return DrawerHeader(
//         margin: EdgeInsets.zero,
//         padding: EdgeInsets.zero,
//         decoration: BoxDecoration(color: Color(0xFF0099FF)),
//         child: Stack(children: <Widget>[
//           Positioned(
//             top: 25.0,
//             left: 20.0,
//             child: Text(
//               'Memory Life',
//               style: TextStyle(
//                   fontWeight: FontWeight.bold,
//                   fontSize: 22,
//                   color: Colors.white),
//             ),
//           ),
//           Positioned(
//             top: 80.0,
//             left: 20.0,
//             child: GestureDetector(
//               onTap: () {
//                 widget.account(page: 4);
//                 Navigator.pop(context);
//               },
//               child: Container(
//                 height: 44,
//                 width: 44,
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.circular(500),
//                   child: Image.network(
//                     state?.user?.avatar ?? '',
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             top: 90.0,
//             left: 75.0,
//             child: state.user.name != ''
//                 ? Text(
//                     state?.user?.name ?? '',
//                     style: TextStyle(fontSize: 12, color: Colors.white),
//                   )
//                 : Text(
//                     state?.user?.email ?? '',
//                     style: TextStyle(fontSize: 12, color: Colors.white),
//                   ),
//           ),
//         ]));
//   }
//
//   Widget _createDrawerItem(
//       {AssetImage icon, String text, GestureTapCallback onTap}) {
//     return ListTile(
//       title: Row(
//         children: <Widget>[
//           ImageIcon(
//             icon,
//             color: Colors.blue,
//           ),
//           Padding(
//             padding: EdgeInsets.only(left: 8.0),
//             child: Text(
//               text,
//               style: TextStyle(color: Colors.black),
//             ),
//           )
//         ],
//       ),
//       onTap: onTap,
//       dense: true,
//     );
//   }
// }
