import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/model/entity/order.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/screen/ordered/list_order.dart';
import 'package:foodsmy/presentation/screen/ordered/widget_order_appbar.dart';

class OrderScreen extends StatefulWidget {
  @override
  OrderScreenState createState() => OrderScreenState();
}

class OrderScreenState extends State<OrderScreen> {
  Item selectedUser;
  Item selected;
  int type;
  List<Item> items = <Item>[
    const Item('Doanh số theo ngày', "assets/images/img_filter.png", 1),
    const Item('Doanh số theo tháng', "assets/images/img_filter.png", 2),
  ];
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return Scaffold(
      body: Column(
        children: [
          _buildOrderApbar(),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  decoration: BoxDecoration(
                    color: AppColor.PRIMARY_COLOR,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text(
                    "Tổng: 2000000 đ",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
                DropdownButtonHideUnderline(
                  child: DropdownButton<Item>(
                    icon: Icon(
                      Icons.filter_alt_outlined,
                      color: AppColor.PRIMARY_COLOR,
                    ),
                    hint: Text(items.first.name),
                    iconSize: 24,
                    elevation: 16,
                    isExpanded: false,
                    value: selected,
                    onChanged: (Item item) {
                      setState(() {
                        selected = item;
                      });
                    },
                    items: items.map((Item user) {
                      return DropdownMenuItem<Item>(
                        value: user,
                        child: Text(
                          user.name,
                          style: TextStyle(color: Colors.black),
                        ),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: itemOrder.length,
            physics: BouncingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return ListOrder(
                id: index,
              );
            },
          ))
        ],
      ),
    );
  }

  _buildOrderApbar() => WidgetOrderAppbar();
}

class Item {
  const Item(this.name, this.icon, this.id);

  final String name;
  final String icon;
  final int id;
}
