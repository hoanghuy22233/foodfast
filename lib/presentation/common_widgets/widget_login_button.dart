import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';
import 'package:foodsmy/app/constants/style/style.dart';
import 'package:foodsmy/app/constants/value/value.dart';

class WidgetLoginButton extends StatelessWidget {
  final Function onTap;
  final String text;
  final isEnable;

  const WidgetLoginButton({Key key, this.onTap, this.text, this.isEnable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2.5,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: isEnable ? AppColor.BUTTON_ENABLE_COLOR : AppColor.BUTTON_DISABLE_COLOR,
          child: Center(
              child: Text(
            text,
            style: isEnable
                ? AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.WHITE)
                : AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.BLACK),
          )),
        ),
      ),
    );
  }
}
