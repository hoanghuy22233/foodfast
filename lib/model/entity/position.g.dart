// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'position.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Positions _$PositionsFromJson(Map<String, dynamic> json) {
  return Positions(
    (json['lat'] as num)?.toDouble(),
    (json['long'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$PositionsToJson(Positions instance) => <String, dynamic>{
      'lat': instance.lat,
      'long': instance.long,
    };
