import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_button.dart';
import 'package:foodsmy/presentation/common_widgets/widget_login_input.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/utils/common/common_utils.dart';
import 'package:foodsmy/utils/snackbar/get_snack_bar_utils.dart';
import 'package:get/get.dart';

import 'bloc/bloc.dart';

class WidgetForgotPasswordResetForm extends StatefulWidget {
  final String username;
  final String otpCode;

  const WidgetForgotPasswordResetForm({Key key, this.username, this.otpCode})
      : super(key: key);

  @override
  _WidgetForgotPasswordResetFormState createState() =>
      _WidgetForgotPasswordResetFormState();
}

class _WidgetForgotPasswordResetFormState
    extends State<WidgetForgotPasswordResetForm> {
  ForgotPasswordResetBloc _forgotPasswordBloc;

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  bool autovalidate = false;
  bool obscurePasswordOne = true;
  bool obscurePasswordTwo = true;
  bool obscureConfirmPassword = true;

  bool get isPopulated =>
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordResetBloc>(context);
    _passwordController.addListener(_onPasswordChange);
    _confirmPasswordController.addListener(_onConfirmPasswordChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordResetBloc, ForgotPasswordResetState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateLogin();
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autovalidate = true;
          });
        }
      },
      child: BlocBuilder<ForgotPasswordResetBloc, ForgotPasswordResetState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 25,
                  ),
                  _buildButtonForgotPasswordReset(state)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isForgotPasswordResetButtonEnabled() {
    return _forgotPasswordBloc.state.isFormValid &&
        isPopulated &&
        !_forgotPasswordBloc.state.isSubmitting;
  }

  _buildButtonForgotPasswordReset(ForgotPasswordResetState state) {
    return WidgetLoginButton(
      onTap: () {
        // if (isForgotPasswordResetButtonEnabled()) {
        //   _forgotPasswordBloc.add(ForgotPasswordResetSubmitted(
        //       username: widget.username,
        //       otpCode: widget.otpCode,
        //       password: _passwordController.text,
        //       confirmPassword: _confirmPasswordController.text));
        //   AppCommonUtils.disposeKeyboard();
        // }

        AppNavigator.navigateLogin();

      },
      isEnable: isForgotPasswordResetButtonEnabled(),
      text: "Thay đổi",
    );
  }

  _buildTextFieldConfirmPassword() {
    return WidgetLoginInput(
      inputController: _confirmPasswordController,
      obscureText: obscurePasswordTwo,
      autovalidate: autovalidate,
      validator: (_) {
        return !_forgotPasswordBloc.state.isConfirmPasswordValid
            ? "Nhập lại mật khẩu của bạn"
            : null;
      },
      hint: "Nhập lại mật khẩu *",
      leadIcon: new Icon(
        Icons.lock_outline,
        color: AppColor.PRIMARY_COLOR,
      ),
      endIcon: IconButton(
        icon: Icon(
          obscurePasswordTwo
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePasswordTwo = !obscurePasswordTwo;
          });
        },
      ),
    );
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      inputController: _passwordController,
      obscureText: obscurePasswordOne,
      autovalidate: autovalidate,
      validator: (_) {
        return !_forgotPasswordBloc.state.isPasswordValid
            ? "Nhập mật khẩu của bạn"
            : null;
      },
      hint: "Mật khẩu *",
      leadIcon: new Icon(
        Icons.lock_outline,
        color: AppColor.PRIMARY_COLOR,
      ),
      endIcon: IconButton(
        icon: Icon(
          obscurePasswordOne
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePasswordOne = !obscurePasswordOne;
          });
        },
      ),
    );
  }

  void _onPasswordChange() {
    _forgotPasswordBloc.add(PasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onConfirmPasswordChange() {
    _forgotPasswordBloc.add(ConfirmPasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }
}
