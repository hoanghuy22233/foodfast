import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'new.g.dart';

@JsonSerializable()
class News extends Equatable {
  int id;
  @JsonKey(name: "title")
  String title;
  @JsonKey(name: "content")
  String content;
  @JsonKey(name: "type_status")
  int typeStatus;
  @JsonKey(name: "cotype_workntent")
  int coTypeWork;
  @JsonKey(name: "image")
  String image;
  @JsonKey(name: "created_at")
  String date;
  @JsonKey(name: "lat")
  double lat;
  @JsonKey(name: "long")
  double long;

  News(this.id, this.title, this.content, this.typeStatus, this.coTypeWork,
      this.image, this.date, this.lat, this.long);

  factory News.fromJson(Map<String, dynamic> json) => _$NewsFromJson(json);

  Map<String, dynamic> toJson() => _$NewsToJson(this);

  @override
  List<Object> get props =>
      [id, title, content, typeStatus, coTypeWork, image, date, lat, long];

  @override
  String toString() {
    return 'New{id:$id, title:$title,content:$content,typeStatus:$typeStatus,coTypeWork:$coTypeWork,image:$image,date:$date,lat:$lat,long:$long }';
  }
}
enum Places {
  favorite,
  visited,
  wantToGo,
}
