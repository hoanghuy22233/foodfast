class Endpoint {
  Endpoint._();

  static const BASE_URL = 'http://memorylife.xyz';
  //AUTH ROUTE
  static const LOGIN_APP = '/api/login';
  static const REGISTER_APP = '/api/register';
  static const FORGOT_PASSWORD = '/api/forgotPass';
  static const FORGOT_PASSWORD_VERIFY = '/api/checkOtpInForgotPassword';
  static const FORGOT_PASSWORD_RESET = '/api/changeForgotPassword';
  static const NEWS_APP = '/api/listDiary';
  static const PROFILE = '/api/profile';
  static const NEWS_DETAIL = '/api/getDetailDiary';
  static const REMOVE_POST = '/api/deleteDiary';
  static const UPDATE_INFORMATION = '/api/updateProfile';
  static const UPDATE_BACKGROUND = '/api/updateBackgroundImage';
  static const UPDATE_AVATAR = '/api/updateAvatar';
  static const POST_DIARY = '/api/postDiary';
  static const CHANGE_PASS = '/api/updatePassword';
  static const EDIT_DIARY = '/api/editDiary';
  static const GET_IMAGE = '/api/getListImageDiary';
  static const UPDATE_LOCATION = '/api/updateLocation';
  static const GET_ALL_LOCATION = '/api/getAllLatLongUser';

  static const int DEFAULT_LIMIT = 20;

  // request failed
  static const int FAILURE = 0;

  // request success
  static const int SUCCESS = 1;

  // request with token expire
  static const int TOKEN_EXPIRE = 2;

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 15000;

  // method
  static const GET = 'GET';
  static const POST = 'POST';
  static const PUT = 'PUT';
  static const DELETE = 'DELETE';

  // get path
  static String getPath(String path) {
    return '$BASE_URL$path';
  }
}
