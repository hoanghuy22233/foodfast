import 'package:flutter/material.dart';
import 'package:foodsmy/model/entity_offline/drinks.dart';
import 'package:foodsmy/model/entity_offline/favorite.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_cart_appbar_menu.dart';
import 'package:foodsmy/presentation/screen/all_product/poroduct_grid_item_no_check.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';

class FavoriteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _appbar(),
            _buildFood(MediaQuery.of(context).size.height*0.7),
          ],
        ),
      ),
    );
  }


  _appbar() => WidgetAppbar(
    title: "Món yêu thích",
    right: [
      WidgetCartAppBarMenu()
    ],
  );

  Widget _buildFood(double height){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Món yêu thích", style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
          SizedBox(height: 10,),
          Container(
            height: height != null ? height : 400,
            child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: 0.7,
              children: List.generate(favoriteItem.length, (index) {
                return ProductGridItemNocheck(index,favoriteItem);
              }),
            ),
          ),
        ],
      ),
    );
  }
}
