class FavoriteItem {
  final int id;
  final String name;
  final String image;
  final double price;
  final String introduce;
  final String details;
  final bool isFavorite;

  FavoriteItem({this.id,this.name, this.image, this.price, this.introduce, this.details, this.isFavorite});

}

List<FavoriteItem> favoriteItem = [
  FavoriteItem(
    id: 1,
    name: 'Trộn thập cẩm',
    introduce: 'Ngon bổ rẻ!',
    price: 25000,
    image: 'assets/images/thap_cam.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  FavoriteItem(
    id: 2,
    name: 'Trứng cút nướng phô mai - bơ',
    introduce: 'Ngon bổ rẻ!',
    price: 25000,
    image: 'assets/images/cut_xao_me.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  FavoriteItem(
    id: 3,
    name: 'Trứng cút sốt me - bơ',
    introduce: 'Ngon bổ rẻ!',
    price: 25000,
    image: 'assets/images/cut_ap_chao.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  FavoriteItem(
    id: 4,
    name: 'Nem rán Hà Nội',
    introduce: 'Ngon bổ rẻ!',
    price: 30000,
    image: 'assets/images/nem.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  FavoriteItem(
    id: 5,
    name: '7 Up',
    introduce: 'Ngon bổ rẻ!',
    price: 12000,
    image: 'assets/images/7up.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  FavoriteItem(
    id: 6,
    name: 'Trà sâm dứa',
    introduce: 'Ngon bổ rẻ!',
    price: 3000,
    image: 'assets/images/tra_sam_dua.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: true,
  ),
  
];

