import 'package:equatable/equatable.dart';
import 'package:foodsmy/model/entity/user.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_data.g.dart';

@JsonSerializable()
class LoginData extends Equatable {
  @JsonKey(name: "token")
  String token;

  @JsonKey(name: "user")
  User user;

  LoginData({this.token, this.user});

  factory LoginData.fromJson(Map<String, dynamic> json) =>
      _$LoginDataFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataToJson(this);

  @override
  List<Object> get props => [token, user];

  @override
  String toString() {
    return 'LoginData{token: $token, user: $user}';
  }
}
