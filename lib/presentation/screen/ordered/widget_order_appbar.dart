import 'package:flutter/material.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';

class WidgetOrderAppbar extends StatefulWidget {
  @override
  _WidgetOrderAppbarState createState() => _WidgetOrderAppbarState();
}

class _WidgetOrderAppbarState extends State<WidgetOrderAppbar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Doanh thu",
      ),
    );
  }
}
