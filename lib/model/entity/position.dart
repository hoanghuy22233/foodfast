import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'position.g.dart';

@JsonSerializable()
class Positions extends Equatable {
  @JsonKey(name: "lat")
  double lat;
  @JsonKey(name: "long")
  double long;

  Positions(this.lat, this.long);

  factory Positions.fromJson(Map<String, dynamic> json) => _$PositionsFromJson(json);

  Map<String, dynamic> toJson() => _$PositionsToJson(this);

  @override
  List<Object> get props => [lat, long];

  @override
  String toString() {
    return 'Position {lat:$lat, long:$long }';
  }
}
