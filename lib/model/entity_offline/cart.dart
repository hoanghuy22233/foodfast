import 'package:foodsmy/model/entity_offline/drinks.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';

class CartItem {
  final int id;
  final List name;
  final int quantity;

  CartItem({this.id, this.name, this.quantity});

}

List<CartItem> cartItem = [
  CartItem(
    id: 1,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 3,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 9,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 4,
    name: drinkItem,
    quantity: 2,
  ),
  CartItem(
    id: 1,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 3,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 9,
    name: foodItem,
    quantity: 2,
  ),
  CartItem(
    id: 4,
    name: drinkItem,
    quantity: 2,
  ),
];

