import 'package:flutter/material.dart';
import 'package:foodsmy/presentation/screen/profile/widget_account_appbar.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'body.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            color: AppColor.PRIMARY_COLOR,
            padding: EdgeInsets.only(top: 20),
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          Expanded(
            child: Body()
          )
        ],
      ),
    );
  }

  _buildAppbar() => WidgetAccountAppbar();
}
