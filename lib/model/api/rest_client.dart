import 'dart:io';

import 'package:dio/dio.dart';
import 'package:foodsmy/app/constants/endpoint/app_endpoint.dart';
import 'package:foodsmy/model/api/request/change_app_request.dart';
import 'package:foodsmy/model/api/request/delete_post_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_reset_request.dart';
import 'package:foodsmy/model/api/request/forgot_password_verify_request.dart';
import 'package:foodsmy/model/api/request/register_app_request.dart';
import 'package:foodsmy/model/api/request/update_birthday_request.dart';
import 'package:foodsmy/model/api/request/update_email_request.dart';
import 'package:foodsmy/model/api/request/update_name_request.dart';
import 'package:foodsmy/model/api/request/update_phone_request.dart';
import 'package:foodsmy/model/api/response/forgot_password_reset_response.dart';
import 'package:foodsmy/model/api/response/forgot_password_response.dart';
import 'package:foodsmy/model/api/response/forgot_password_verify_response.dart';
import 'package:foodsmy/model/api/response/images_response.dart';
import 'package:foodsmy/model/api/response/location_response.dart';
import 'package:foodsmy/model/api/response/login_response.dart';
import 'package:foodsmy/model/api/response/news_detail_response.dart';
import 'package:foodsmy/model/api/response/news_response.dart';
import 'package:foodsmy/model/api/response/post_diary_response.dart';
import 'package:foodsmy/model/api/response/profile_response.dart';
import 'package:foodsmy/model/api/response/update_avatar_response.dart';
import 'package:foodsmy/model/api/response/update_background_image_response.dart';
import 'package:foodsmy/model/api/response/update_birthday_response.dart';
import 'package:foodsmy/model/api/response/update_email_response.dart';
import 'package:foodsmy/model/api/response/update_gender_response.dart';
import 'package:foodsmy/model/api/response/update_location_response.dart';
import 'package:foodsmy/model/api/response/update_name_response.dart';
import 'package:foodsmy/model/api/response/update_phone_response.dart';
import 'package:retrofit/retrofit.dart';

import 'request/login_app_request.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: Endpoint.BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST(Endpoint.LOGIN_APP)
  Future<LoginResponse> loginApp(@Body() LoginAppRequest loginAppRequest);

  @POST(Endpoint.REGISTER_APP)
  Future<LoginResponse> registerApp(
      @Body() RegisterAppRequest registerAppRequest);

  @POST(Endpoint.CHANGE_PASS)
  Future<LoginResponse> changePassWord(
      @Body() ChangeAppRequest changeAppRequest);

  @POST(Endpoint.FORGOT_PASSWORD)
  Future<ForgotPasswordResponse> forgotPassword(
      @Body() ForgotPasswordRequest forgotPasswordRequest);

  @POST(Endpoint.FORGOT_PASSWORD_VERIFY)
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      @Body() ForgotPasswordVerifyRequest forgotPasswordVerifyRequest);

  @POST(Endpoint.FORGOT_PASSWORD_RESET)
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      @Body() ForgotPasswordResetRequest forgotPasswordResetRequest);

  @GET(Endpoint.NEWS_APP)
  Future<NewsResponse> getNews();
  @GET(Endpoint.GET_IMAGE)
  Future<ImagesResponse> getImages();

  @GET(Endpoint.PROFILE)
  Future<ProfileResponse> getProfile();

  @GET(Endpoint.GET_ALL_LOCATION)
  Future<LocationResponse> getPosition();

  @GET(Endpoint.NEWS_DETAIL)
  Future<NewsDetailResponse> getNewsDetail(
    @Query("diary_id") int newsId,
  );

  @POST(Endpoint.REMOVE_POST)
  Future<NewsResponse> removePost(@Body() DeletePostRequest deletePostRequest);

  @POST(Endpoint.UPDATE_INFORMATION)
  Future<UpdateNameResponse> updateName(
      @Body() UpdateNameRequest updateNameRequest);

  @POST(Endpoint.UPDATE_INFORMATION)
  Future<UpdatePhoneResponse> updatePhone(
      @Body() UpdatePhoneRequest updatePhoneRequest);

  @POST(Endpoint.UPDATE_INFORMATION)
  Future<UpdateEmailResponse> updateEmail(
      @Body() UpdateEmailRequest updateEmailRequest);

  @POST(Endpoint.UPDATE_INFORMATION)
  Future<UpdateBirthdayResponse> updateBirthday(
      @Body() UpdateBirthdayRequest updateBirthdayRequest);

  @POST(Endpoint.UPDATE_AVATAR)
  Future<UpdateAvatarResponse> updateAvatar(
      @Part(name: 'avatar') File avatarFile);

  @POST(Endpoint.UPDATE_BACKGROUND)
  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      @Part(name: 'background_image') File backgroundImageFile);

  @POST(Endpoint.UPDATE_INFORMATION)
  Future<UpdateGenderResponse> genDer(
    @Part(name: 'gender') int superId,
  );

  @POST(Endpoint.UPDATE_LOCATION)
  Future<UpdateLocationResponse> updateLocation(
    @Part(name: "lat") String lat,
    @Part(name: "long") String long,
  );

  @POST(Endpoint.POST_DIARY)
  Future<PostDiaryResponse> postDiary(
    @Part(name: "image") File storeImages,
    @Part(name: "title") String name,
    @Part(name: "content") String description,
    @Part(name: "lat") double lat,
    @Part(name: "long") double long,
    @Part(name: "type_status") int icon,
    @Part(name: "type_work") int typeWork,
  );
  @POST(Endpoint.POST_DIARY)
  Future<PostDiaryResponse> postDiaryNoImage(
    @Part(name: "title") String name,
    @Part(name: "content") String description,
    @Part(name: "lat") double lat,
    @Part(name: "long") double long,
    @Part(name: "type_status") int icon,
    @Part(name: "type_work") int typeWork,
  );
  @POST(Endpoint.EDIT_DIARY)
  Future<PostDiaryResponse> editDiary(
    @Part(name: "diary_id") int id,
    @Part(name: "image") File storeImages,
    @Part(name: "title") String name,
    @Part(name: "content") String description,
    @Part(name: "lat") double lat,
    @Part(name: "long") double long,
    @Part(name: "type_status") int icon,
    @Part(name: "type_work") int typeWork,
  );

  @POST(Endpoint.EDIT_DIARY)
  Future<PostDiaryResponse> editDiaryNoImage(
    @Part(name: "diary_id") int id,
    @Part(name: "title") String name,
    @Part(name: "content") String description,
    @Part(name: "lat") double lat,
    @Part(name: "long") double long,
    @Part(name: "type_status") int icon,
    @Part(name: "type_work") int typeWork,
  );
}
