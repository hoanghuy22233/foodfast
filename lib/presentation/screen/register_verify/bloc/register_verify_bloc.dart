import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/endpoint/app_endpoint.dart';
import 'package:foodsmy/app/constants/preferences/app_preferences.dart';
import 'package:foodsmy/model/local/pref.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/utils/validator/validator.dart';
import 'package:meta/meta.dart';

import 'bloc.dart';

class RegisterVerifyBloc
    extends Bloc<RegisterVerifyEvent, RegisterVerifyState> {
  final UserRepository _userRepository;

  RegisterVerifyBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterVerifyState get initialState => RegisterVerifyState.empty();

  @override
  Stream<RegisterVerifyState> mapEventToState(
      RegisterVerifyEvent event) async* {
    if (event is OtpCodeChanged) {
      yield* _mapOtpCodeChangeToState(event.otpCode);
    } else if (event is RegisterVerifySubmitted) {
      yield* _mapFormSubmittedToState(event.username, event.otpCode);
    }
  }

  Stream<RegisterVerifyState> _mapOtpCodeChangeToState(String otpCode) async* {
    yield state.update(isOtpCodeValid: Validator.isValidOtp(otpCode));
  }

  Stream<RegisterVerifyState> _mapFormSubmittedToState(
      String username, String otpCode) async* {
    yield RegisterVerifyState.loading();

    //need refactor
    var isOtpCodeValid = Validator.isValidOtp(otpCode);

    var newState = state.update(
      isOtpCodeValid: isOtpCodeValid,
    );

    yield newState;

    // if (newState.isFormValid) {
    //   try {
    //     var response = await _userRepository.registerVerify(
    //         username: username, otpCode: otpCode);
    //     if (response.status == Endpoint.SUCCESS) {
    //       final prefs = LocalPref();
    //       await prefs.saveBool(AppPreferences.new_register, true);
    //       yield RegisterVerifyState.success(message: response.message);
    //     } else {
    //       yield RegisterVerifyState.failure(message: response.message);
    //     }
    //   } catch (e) {
    //     print(e.toString());
    //     yield RegisterVerifyState.failure(message: e.toString());
    //   }
    // }
  }
}
