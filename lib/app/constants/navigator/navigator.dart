import 'package:foodsmy/model/entity/new.dart';
import 'package:foodsmy/presentation/router.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.toNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD);
    return result;
  }


  static navigateForgotPasswordVerify({String username}) async {
    var result =
    await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY, arguments: {
      'username': username,
    });
    return result;
  }

  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
    await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }

  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }

  static navigateCart() async {
    var result = await Get.toNamed(BaseRouter.CART);
    return result;
  }


// static navigateNewsDetail(int id) async {
//   var result =
//       await Get.toNamed(BaseRouter.NEWS_DETAIL, arguments: {'id': id});
//   return result;
// }
}
