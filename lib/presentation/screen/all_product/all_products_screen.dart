import 'package:flutter/material.dart';
import 'package:foodsmy/model/entity_offline/drinks.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_cart_appbar_menu.dart';
import 'package:foodsmy/presentation/screen/all_product/poroduct_grid_item_no_check.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';

class AllProductsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _appbar(),
            _buildFood(MediaQuery.of(context).size.height*0.4),
            SizedBox(height: 20,),
            _buildDrink(MediaQuery.of(context).size.height*0.4),
          ],
        ),
      ),
    );
  }


  _appbar() => WidgetAppbar(
    title: "Menu",
    right: [
      WidgetCartAppBarMenu()
    ],
  );

  Widget _buildFood(double height){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Đồ ăn", style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
          SizedBox(height: 10,),
          Container(
            height: height != null ? height : 400,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: foodItem.length,
                itemBuilder: (context, index){
                  return ProductGridItemNocheck(index, foodItem);
            }),
          ),
        ],
      ),
    );
  }

  Widget _buildDrink(double height){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Đồ uống", style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
          SizedBox(height: 10,),
          Container(
            height: height != null ? height : 400,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: drinkItem.length,
                itemBuilder: (context, index){
                  return ProductGridItemNocheck(index, drinkItem);
                }),
          ),
        ],
      ),
    );
  }
}
