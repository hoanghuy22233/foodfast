import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/screen/forgot_password/barrel_forgot_password.dart';
import 'package:foodsmy/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:foodsmy/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:foodsmy/presentation/screen/login/login.dart';
import 'package:foodsmy/presentation/screen/register/sc_register.dart';
import 'package:foodsmy/presentation/screen/navigation/sc_navigation.dart';
import 'package:foodsmy/presentation/screen/splash/splash.dart';
import 'package:foodsmy/presentation/screen/cart/cart_screen.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String NAVIGATION = '/navigation';
  static const String REGISTER = '/register';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String CART = '/cart';




  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      REGISTER: (context) => RegisterScreen(),
      FORGOT_PASSWORD: (context) => ForgotPassword(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      CART: (context) => CartScreen(),

      NAVIGATION: (context) => NavigationScreen(),
    };
  }
}
