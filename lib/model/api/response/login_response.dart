import 'package:foodsmy/model/entity/login_data.dart';
import 'package:json_annotation/json_annotation.dart';

import 'base_response.dart';

part 'login_response.g.dart';

@JsonSerializable()
class LoginResponse extends BaseResponse {
  LoginData data;

  LoginResponse(this.data);

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);

  @override
  List<Object> get props => [data];

  @override
  String toString() {
    return 'LoginResponse{data: $data}';
  }
}
