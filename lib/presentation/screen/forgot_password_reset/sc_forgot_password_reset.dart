import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:foodsmy/presentation/screen/forgot_password_reset/widget_forgot_password_reset_form.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

import 'bloc/forgot_password_reset_bloc.dart';

class ForgotPasswordResetScreen extends StatefulWidget {
  @override
  _ForgotPasswordResetScreenState createState() =>
      _ForgotPasswordResetScreenState();
}

class _ForgotPasswordResetScreenState extends State<ForgotPasswordResetScreen> {
  String _username;
  String _otpCode;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        _otpCode = arguments['otp_code'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: BlocProvider(
              create: (context) =>
                  ForgotPasswordResetBloc(userRepository: userRepository),
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      //mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _buildAppbar(),
                        SizedBox(height: MediaQuery.of(context).size.height/4,),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Text(
                              AppLocalizations.of(context).translate('forgot_password_reset.message'),
                              style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        WidgetSpacer(
                          height: 10,
                        ),
                        _buildForgotPasswordResetForm(),
                        WidgetSpacer(
                          height: 20,
                        ),
                      ],
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbar(
    left: [WidgetAppbarMenuBack()],
    title: AppLocalizations.of(context).translate('forgot_password_reset.title'),
  );

  _buildForgotPasswordResetForm() => WidgetForgotPasswordResetForm(
        username: _username,
        otpCode: _otpCode,
      );
}
