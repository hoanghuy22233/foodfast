import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:foodsmy/model/entity/categorys.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_main.dart';
import 'package:foodsmy/presentation/screen/home_page/category.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return Scaffold(
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 3,
                  width: MediaQuery.of(context).size.width,
                  child: Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return new Image.asset(
                        "assets/images/banner_three.jpg",
                        fit: BoxFit.fill,
                      );
                    },
                    pagination: new SwiperPagination(
                      alignment: Alignment.bottomCenter,
                      builder: new DotSwiperPaginationBuilder(
                          color: Colors.grey, activeColor: Colors.blue),
                    ),

                    autoplay: true,
                    itemCount: 3,
                    scrollDirection: Axis.horizontal,
                    // control: new SwiperControl(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                _buildContent(),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/table.png",
                        height: 40,
                        width: 40,
                      ),
                      Text("Đang trống",
                          style: TextStyle(color: Colors.black, fontSize: 12)),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/table.png",
                        height: 40,
                        width: 40,
                        color: Colors.red,
                      ),
                      Text(
                        "Đã có khách",
                        style: TextStyle(color: Colors.black, fontSize: 12),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 100,
                )
              ],
            ),
          ),
          _buildLogoAndCart()
        ],
      ),
    );
  }

  _buildLogoAndCart() => WidgetAppBarMain(
        openDrawer: () {
          //  widget.drawer.currentState.openDrawer();
        },
        color: Colors.transparent,
        heroSearch: true,
      );
  Widget _buildContent() {
    return Container(
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xfff2f2f2),
                borderRadius: BorderRadius.circular(20)),
            child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Category(
                      id: index,
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 6,
                    crossAxisSpacing: 0,
                    mainAxisSpacing: 5,
                    childAspectRatio: 0.8),
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemCategory.length),
          )),
    );
  }
}
