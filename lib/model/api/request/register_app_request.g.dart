// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterAppRequest _$RegisterAppRequestFromJson(Map<String, dynamic> json) {
  return RegisterAppRequest(
    emailOrPhone: json['email'] as String,
    password: json['password'] as String,
    confirmPassword: json['confirm_password'] as String,
  );
}

Map<String, dynamic> _$RegisterAppRequestToJson(RegisterAppRequest instance) =>
    <String, dynamic>{
      'email': instance.emailOrPhone,
      'password': instance.password,
      'confirm_password': instance.confirmPassword,
    };
