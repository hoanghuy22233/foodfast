// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_post_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeletePostRequest _$DeletePostRequestFromJson(Map<String, dynamic> json) {
  return DeletePostRequest(
    json['diary_id'] as int,
  );
}

Map<String, dynamic> _$DeletePostRequestToJson(DeletePostRequest instance) =>
    <String, dynamic>{
      'diary_id': instance.id,
    };
