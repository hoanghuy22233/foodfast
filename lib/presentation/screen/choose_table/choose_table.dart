import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/presentation/screen/home_page/category.dart';
import 'package:foodsmy/model/entity/categorys.dart';

class ChooseScreen extends StatefulWidget {
  @override
  _ChooseScreenState createState() => _ChooseScreenState();
}

class _ChooseScreenState extends State<ChooseScreen> {


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  "Chọn bàn",
                  style: AppStyle.DEFAULT_LARGE_BOLD,
                ),
              ),
              GestureDetector(
                onTap: (){

                },
                child: Container(
                  width: 25,
                  height: 25,
                  child: Image.asset('assets/images/img_close_round.png'),
                ),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          _buildChooseTable(),
          SizedBox(
            height: 10,
          ),

        ],
      ),
    );
  }

  _buildChooseTable(){
    return Container(
      child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xfff2f2f2),
                borderRadius: BorderRadius.circular(20)),
            child: GridView.builder(
                shrinkWrap: true,
                padding: EdgeInsets.only(top: 10, left: 5, right: 5),
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Category(
                      id: index,
                    ),
                  );
                },
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 6,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 5,
                    childAspectRatio: 0.85),
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemCategory.length),
          )),
    );
  }
}
