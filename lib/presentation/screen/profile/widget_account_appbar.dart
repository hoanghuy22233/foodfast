import 'package:flutter/material.dart';
import 'package:foodsmy/presentation/screen/profile/widget_appbar_account_main.dart';

class WidgetAccountAppbar extends StatelessWidget {
  final String titleText;

  const WidgetAccountAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarAccountMain(
        title: "Tài khoản",
        right: [
          FlatButton(
            onPressed: () {},
            child: Text(
              "Sửa",
              style: TextStyle(
                color: Colors.white,
                fontSize: 14, //16
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
