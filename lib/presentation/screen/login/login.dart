import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/auth_bloc/authentication_bloc.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:foodsmy/presentation/screen/login/widget_login_form.dart';

import '../../../app/constants/color/color.dart';
import '../../../app/constants/style/style.dart';
import 'bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Bạn chắc chứ?'),
            content: Text('Bạn muốn thoát ứng dụng?'),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text('Không'),
              ),
              FlatButton(
                onPressed: () => exit(0),
                /*Navigator.of(context).pop(true)*/
                child: Text('Có'),
              ),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: WillPopScope(
          onWillPop: _onWillPop,
          child: BlocProvider(
            create: (context) => LoginBloc(
                userRepository: userRepository,
                authenticationBloc:
                    BlocProvider.of<AuthenticationBloc>(context)),
            child: Stack(
              alignment: Alignment.center,
              children: [
                SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      WidgetSpacer(
                        height: MediaQuery.of(context).size.height/4,
                      ),
                      Text('Bánh tráng trộn'.toUpperCase(), style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
                      SizedBox(height: 20,),
                      Text('50B - Lương Khánh Thiện', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),),
                      WidgetSpacer(
                        height: 40,
                      ),
                      _buildLoginForm(),
                      Center(
                        child: RichText(
                          text: TextSpan(
                              text: 'Bạn chưa có tài khoản?  ',
                              style: TextStyle(color: Colors.black),
                              children: <TextSpan>[
                                TextSpan(
                                    text: 'Đăng ký',
                                    style: TextStyle(
                                        color: AppColor.PRIMARY_COLOR,
                                        fontWeight: FontWeight.bold,
                                        decoration: TextDecoration.underline),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        AppNavigator.navigateRegister();
                                      })
                              ]),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildLoginForm() => WidgetLoginForm();
}
