class OrderItem {
  final String image, name, colorImage, background, total, table, sale, code;

  OrderItem(
      {this.image,
      this.name,
      this.colorImage,
      this.background,
      this.total,
      this.table,
      this.sale,
      this.code});
}

List<OrderItem> itemOrder = [
  OrderItem(
      code: "001",
      table: "01",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "5 %"),
  OrderItem(
      code: "002",
      table: "02",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "5 %"),
  OrderItem(
      code: "003",
      table: "03",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "5 %"),
  OrderItem(
      code: "004",
      table: "04",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "6 %"),
  OrderItem(
      code: "005",
      table: "05",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "6 %"),
  OrderItem(
      code: "006",
      table: "06",
      image: "assets/images/banner_three.jpg",
      colorImage: "2ca94f",
      total: "1000000 đ",
      sale: "6 %"),
];
