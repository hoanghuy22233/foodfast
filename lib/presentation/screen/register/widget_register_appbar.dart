import 'package:flutter/material.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

class WidgetRegisterAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: AppLocalizations.of(context).translate('register.title'),
        left: [WidgetAppbarMenuBack()],
      ),
    );
  }
}
