import 'package:flutter/material.dart';

import 'info.dart';
import 'profile_menu_item.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Info(
            image: "assets/images/image4.jpg",
            name: "Bánh tráng trộn",
            email: "50B - Lương Khánh Thiện",
          ),
          SizedBox(height: 10), //20
          ProfileMenuItem(
            iconSrc: "assets/icons/historys.png",
            title: "Lịch sử mua hàng",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/img_heart_actived.png",
            title: "Yêu thích",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/phone.png",
            title: "Liên hệ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/padlock.png",
            title: "Thay đổi mật khẩu",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/exit.png",
            title: "Đăng xuất",
            press: () {},
          ),
        ],
      ),
    );
  }
}
