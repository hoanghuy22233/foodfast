import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/style/style.dart';
import 'package:foodsmy/app/constants/value/value.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';

class WidgetNumberInput extends StatefulWidget {
  final TextEditingController inputController;
  final ValueChanged<String> onChanged;
  final FormFieldValidator<String> validator;
  final String hint;
  final bool obscureText;
  final bool autovalidate;
  final TextInputType inputType;
  final Widget endIcon;
  final Widget leadIcon;

  WidgetNumberInput(
      {this.inputController,
      this.onChanged,
      this.validator,
      this.hint,
      this.inputType = TextInputType.text,
      this.obscureText = false,
      this.autovalidate = false,
      this.endIcon,
      this.leadIcon});

  @override
  _WidgetNumberInputState createState() => _WidgetNumberInputState();
}

class _WidgetNumberInputState extends State<WidgetNumberInput> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Container(
          height: AppValue.INPUT_FORM_HEIGHT,
          width: 100,
          child: Card(
            elevation: 2,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                children: [
                  widget.leadIcon,
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: TextFormField(
                      controller: widget.inputController,
                      onChanged: (change) {
                        widget.onChanged(change);
                      },
                      autovalidate: widget.autovalidate ?? false,
                      validator: widget.validator,
                      style: AppStyle.DEFAULT_MEDIUM,
                      maxLines: 1,
                      keyboardType: widget.inputType ?? TextInputType.text,
                      textAlign: TextAlign.left,
                      obscureText: widget.obscureText,
                      decoration: InputDecoration.collapsed(
                        hintText: widget.hint,
                      ),
                    ),
                  ),
                  widget.endIcon ?? WidgetSpacer()
                ],
              ),
            ),
          ),
    ));
  }
}
