import 'package:foodsmy/model/api/response/base_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'update_gender_response.g.dart';

@JsonSerializable()
class UpdateGenderResponse extends BaseResponse {
  UpdateGenderResponse();

  factory UpdateGenderResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateGenderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateGenderResponseToJson(this);
}
