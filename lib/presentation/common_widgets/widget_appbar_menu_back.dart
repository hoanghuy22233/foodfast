import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/app/constants/value/value.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu.dart';

class WidgetAppbarMenuBack extends StatelessWidget {
  final Function onTap;

  const WidgetAppbarMenuBack({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: AppValue.ACTION_BAR_HEIGHT * 0.2),
        width: AppValue.ACTION_BAR_HEIGHT * 0.8,
        height: AppValue.ACTION_BAR_HEIGHT * 0.8,
        child: WidgetAppbarMenu(
          icon: Image.asset('assets/images/left_arrow.png'),
          onTap: onTap ??
              () {
                AppNavigator.navigateBack();
              },
        ));
  }
}
