import 'package:flutter/cupertino.dart';
import 'package:foodsmy/presentation/common_widgets/widget_snack_bar_notify_delete.dart';
import 'package:get/get.dart';

class DialogDeleteUtils {
  static createNotify(
      {@required String message,
      @required String positiveLabel,
      Function onPositiveTap,
      Function onPositiveTaps,
      @required String negativeLabel,
      @required String negativeLabels,
      Function onNegativeTap}) {
    Get.dialog(
      WidgetSnackBarNotifyDelete(
        message: message,
        positiveLabel: positiveLabel,
        onPositiveTap: onPositiveTap,
        onNegativeTaps: onPositiveTaps,
        negativeLabels: negativeLabels,
        negativeLabel: negativeLabel,
        onTouchOutsizeEnable: true,
        onNegativeTap: onNegativeTap ??
            () {
              Get.back();
            },
      ),
    );
  }
}
