import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/navigator/navigator.dart';
import 'package:foodsmy/presentation/common_widgets/widget_link.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

class WidgetBottomLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Text(
              AppLocalizations.of(context).translate('register.have_account'),
            ),
          ),
          Flexible(
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateLogin();
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: WidgetLink(
                  text:
                      AppLocalizations.of(context).translate('register.login'),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
