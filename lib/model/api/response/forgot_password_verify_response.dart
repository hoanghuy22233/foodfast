import 'package:foodsmy/model/api/response/barrel_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'forgot_password_verify_response.g.dart';

@JsonSerializable()
class ForgotPasswordVerifyResponse extends BaseResponse {
  ForgotPasswordVerifyResponse();

  factory ForgotPasswordVerifyResponse.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordVerifyResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotPasswordVerifyResponseToJson(this);
}
