import 'package:json_annotation/json_annotation.dart';

part 'change_app_request.g.dart';

@JsonSerializable()
class ChangeAppRequest {
  @JsonKey(name: "old_password")
  final String oldPass;
  final String password;
  @JsonKey(name: "confirm_password")
  final String confirmPassword;

  ChangeAppRequest({this.oldPass, this.password, this.confirmPassword});

  factory ChangeAppRequest.fromJson(Map<String, dynamic> json) =>
      _$ChangeAppRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ChangeAppRequestToJson(this);
}
