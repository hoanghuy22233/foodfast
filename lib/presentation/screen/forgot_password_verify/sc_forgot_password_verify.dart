import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/presentation/common_widgets/widget_resend.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:foodsmy/presentation/screen/forgot_password_verify/widget_forgot_password_verify_form.dart';
import 'package:foodsmy/presentation/screen/forgot_password_verify/widget_forgot_password_verify_username.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

import 'bloc/forgot_password_verify_bloc.dart';


class ForgotPasswordVerifyScreen extends StatefulWidget {
  @override
  _ForgotPasswordVerifyScreenState createState() =>
      _ForgotPasswordVerifyScreenState();
}

class _ForgotPasswordVerifyScreenState
    extends State<ForgotPasswordVerifyScreen> {
  String _username;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) =>
                      ForgotPasswordVerifyBloc(userRepository: userRepository),
                ),
              ],
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _buildAppbar(),
                    SizedBox(height: MediaQuery.of(context).size.height/4,),
                    Center(
                      child: _buildUsername(),
                    ),
                    WidgetSpacer(
                      height: 20,
                    ),
                    _buildForm(),
                    WidgetSpacer(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(AppLocalizations.of(context).translate('register_verify.no_code'),
                        style: AppStyle.DEFAULT_MEDIUM,
                        ),
                        SizedBox(width: 10,),
                        _buildResend()
                      ],
                    ),

                  ],
                ),
              )
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbar(
    left: [WidgetAppbarMenuBack()],
    title: AppLocalizations.of(context).translate('forgot_password.title'),
  );

  _buildUsername() => WidgetForgotPasswordVerifyUsername(
        username: _username,
      );

  _buildForm() => WidgetForgotPasswordVerifyForm(username: _username);

  _buildResend() => WidgetResend(
    onTap: (){

    },
    time: 60,
    isValid: true,
  );
}
