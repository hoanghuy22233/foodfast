import 'package:json_annotation/json_annotation.dart';

part 'update_location_request.g.dart';
@JsonSerializable()
class UpdateLocationRequest {
  @JsonKey(name: "lat")
  final double lat;
  @JsonKey(name: "long")
  final double long;

  UpdateLocationRequest({this.lat, this.long});

  factory UpdateLocationRequest.fromJson(Map<String, dynamic> json) =>
      _$UpdateLocationRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateLocationRequestToJson(this);
}