import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/color/color.dart';

class ProfileMenuItem extends StatelessWidget {
  const ProfileMenuItem({
    Key key,
    this.iconSrc,
    this.title,
    this.press,
  }) : super(key: key);
  final String iconSrc, title;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: SafeArea(
            child: Column(
          children: [
            Row(
              children: <Widget>[
                Image.asset(
                  iconSrc,
                  height: 30,
                  width: 30,
                ),
                SizedBox(width: 10),
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 12, //16
                    color: Colors.blue,
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.arrow_forward_ios,
                  size: 12,
                  color: Colors.blue,
                )
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Divider(
                color: AppColor.GREY_LIGHTER_3,
                height: 1,
                thickness: 1,
              ),
            ),
          ],
        )),
      ),
    );
  }
}
