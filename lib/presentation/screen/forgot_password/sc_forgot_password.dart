import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/model/repo/user_repository.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:foodsmy/presentation/common_widgets/widget_spacer.dart';
import 'package:foodsmy/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:foodsmy/presentation/screen/forgot_password/widget_forgot_password_form.dart';
import 'package:foodsmy/utils/locale/app_localization.dart';

import 'bloc/bloc.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key}) : super(key: key);
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: BlocProvider(
            create: (context) =>
                ForgotPasswordBloc(userRepository: userRepository),
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      _buildAppbar(),
                      SizedBox(height: MediaQuery.of(context).size.height/4,),
                      Center(
                        child: Text(
                          AppLocalizations.of(context).translate('forgot_password.message'),
                          style: AppStyle.DEFAULT_LARGE_BOLD.copyWith(color: AppColor.PRIMARY_COLOR),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Center(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            AppLocalizations.of(context).translate('forgot_password.sub_message'),
                            style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.PRIMARY_COLOR),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      WidgetSpacer(
                        height: 10,
                      ),
                      _buildForgotPasswordForm(),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbar(
    left: [WidgetAppbarMenuBack()],
    title: AppLocalizations.of(context).translate('forgot_password.title'),
  );

  _buildForgotPasswordForm() => WidgetForgotPasswordForm();
}
