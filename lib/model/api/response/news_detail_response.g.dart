// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewsDetailResponse _$NewsDetailResponseFromJson(Map<String, dynamic> json) {
  return NewsDetailResponse(
    json['data'] == null
        ? null
        : News.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as int
    ..message = json['message'] as String;
}

Map<String, dynamic> _$NewsDetailResponseToJson(NewsDetailResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };
