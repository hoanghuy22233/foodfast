import 'package:foodsmy/model/api/response/base_response.dart';
import 'package:foodsmy/model/entity/images.dart';
import 'package:json_annotation/json_annotation.dart';

part 'images_response.g.dart';

@JsonSerializable()
class ImagesResponse extends BaseResponse {
  List<Images> data;

  ImagesResponse(this.data);

  factory ImagesResponse.fromJson(Map<String, dynamic> json) =>
      _$ImagesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesResponseToJson(this);
}
