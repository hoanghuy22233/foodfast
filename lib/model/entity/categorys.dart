class CategoryItem {
  final String image, name, colorImage, background;
  CategoryItem({this.image, this.colorImage, this.name, this.background});
}

List<CategoryItem> itemCategory = [
  CategoryItem(
    name: "01",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "02",
    image: "assets/images/table.png",
    colorImage: "ff0c0c",
  ),
  CategoryItem(
    name: "03",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "04",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "05",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "06",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "07",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "08",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "09",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "10",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "11",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "12",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "13",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "14",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "15",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "16",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "17",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "18",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "19",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
  CategoryItem(
    name: "20",
    image: "assets/images/table.png",
    colorImage: "2ca94f",
  ),
];
