import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/style/style.dart';

class WidgetProfileDetailRow extends StatelessWidget {
  final String title;
  final IconData image;
  final String content;
  final Function onTap;

  const WidgetProfileDetailRow(
      {Key key, this.title, this.image, this.content, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            height: 60,
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        image,
                        color: Colors.blue,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        title,
                        style: AppStyle.DEFAULT_SMALL_BOLD,
                      ),
                    ],
                  ),
                ),
                Text(
                  content,
                  style: TextStyle(color: Color(0xff474747), fontSize: 14),
                ),
              ],
            ),
          ),
          Divider(height: 1),
        ],
      ),
    );
  }
}
