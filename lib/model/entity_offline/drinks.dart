class DinksItem {
  final int id;
  final String name;
  final String image;
  final double price;
  final String introduce;
  final String details;
  final bool isFavorite;

  DinksItem({this.id,this.name, this.image, this.price, this.introduce, this.details, this.isFavorite});

}

List<DinksItem> drinkItem = [
  DinksItem(
    id: 1,
    name: 'Sen vàng',
    introduce: 'Ngon bổ rẻ!',
    price: 30000,
    image: 'assets/images/tra_sen_vang.png',
  ),
  DinksItem(
    id: 2,
    name: 'Trà tắc',
    introduce: 'Ngon bổ rẻ!',
    price: 10000,
    image: 'assets/images/tra_tat.png',
  ),
  DinksItem(
    id: 3,
    name: 'Nước chanh',
    introduce: 'Ngon bổ rẻ!',
    price: 15000,
    image: 'assets/images/nuoc_chanh.png',
  ),
  DinksItem(
    id: 4,
    name: 'Mận ngâm',
    introduce: 'Ngon bổ rẻ!',
    price: 15000,
    image: 'assets/images/man_ngam.png',
  ),
  DinksItem(
    id: 5,
    name: 'Nho ngâm',
    introduce: 'Ngon bổ rẻ!',
    price: 15000,
    image: 'assets/images/nho ngam.png',
  ),
  DinksItem(
    id: 6,
    name: 'CoCa',
    introduce: 'Ngon bổ rẻ!',
    price: 12000,
    image: 'assets/images/coca.png',
  ),
  DinksItem(
    id: 7,
    name: 'Sting',
    introduce: 'Ngon bổ rẻ!',
    price: 12000,
    image: 'assets/images/sting.png',
  ),
  DinksItem(
    id: 8,
    name: 'Trà xanh',
    introduce: 'Ngon bổ rẻ!',
    price: 15000,
    image: 'assets/images/tra_xanh_Khong_do.png',
  ),
  DinksItem(
    id: 9,
    name: 'C2',
    introduce: 'Ngon bổ rẻ!',
    price: 12000,
    image: 'assets/images/c2.png',
  ),
  DinksItem(
    id: 10,
    name: '7 Up',
    introduce: 'Ngon bổ rẻ!',
    price: 12000,
    image: 'assets/images/7up.png',
  ),
  DinksItem(
    id: 11,
    name: 'Nước cam',
    introduce: 'Ngon bổ rẻ!',
    price: 15000,
    image: 'assets/images/cam.png',
  ),
  DinksItem(
    id: 12,
    name: 'Bia Hà Nội',
    introduce: 'Ngon bổ rẻ!',
    price: 18000,
    image: 'assets/images/bia_HN.png',
  ),
  DinksItem(
    id: 13,
    name: 'Bò húc',
    introduce: 'Ngon bổ rẻ!',
    price: 18000,
    image: 'assets/images/bo_huc.png',
    details: "Bánh đa, bò khô, trứng cút, rau thơm, lạc, ớt, quất, ",
    isFavorite: false,
  ),
  DinksItem(
    id: 14,
    name: 'Trà sâm dứa',
    introduce: 'Ngon bổ rẻ!',
    price: 3000,
    image: 'assets/images/tra_sam_dua.png',
  ),
  DinksItem(
    id: 15,
    name: 'Nhân trần',
    introduce: 'Ngon bổ rẻ!',
    price: 3000,
    image: 'assets/images/nhan_tran.png',
  ),
];

