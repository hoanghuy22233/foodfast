import 'package:foodsmy/model/api/response/base_response.dart';
import 'package:foodsmy/model/entity/new.dart';
import 'package:json_annotation/json_annotation.dart';

part 'news_detail_response.g.dart';

@JsonSerializable()
class NewsDetailResponse extends BaseResponse {
  News data;

  NewsDetailResponse(this.data);

  factory NewsDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$NewsDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewsDetailResponseToJson(this);
}
