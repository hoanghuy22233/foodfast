import 'package:flutter/material.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/model/entity_offline/foods.dart';
import 'package:foodsmy/presentation/screen/detail_product/detail_products_screen.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';


class ProductGridItemNocheck extends StatelessWidget {
  final int index;
  final List product;

  ProductGridItemNocheck(this.index, this.product);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 0.0, bottom: 10.0, left: 5.0, right: 5.0),
        child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => DetailProductsScreen(index, product)),
            );
          },
          child: Container(
              //height: MediaQuery.of(context).size.height / 3,
              width: 170.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(5.0, 5.0),
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 8.0,
                  ),
                  BoxShadow(
                    offset: Offset(-5.0, -5.0),
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 8.0,
                  ),
                ],
              ),
              child: Column(
                  children: [
                    product[index].isFavorite != null && product[index].isFavorite == true ? Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            height: 30,
                            width: 30,
                            child: IconButton(
                              icon:Icon(Icons.favorite, color: Colors.red, size: 20,)
                            ),
                          ),
                        ]
                    ) : SizedBox(height: 30,),
                    // -------------------------------- Product Image -------------------------------- //
                    Container(
                      height: 85.0,
                      width: MediaQuery.of(context).size.width * 0.3,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(product[index].image),
                            fit: BoxFit.contain),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    // -------------------------------- Product price and title -------------------------------- //
                    Text(AppValue.APP_MONEY_FORMAT.format(product[index].price),
                        style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.green)),
                    SizedBox(height: 10.0),
                    Text(
                      product[index].name.length <= 28
                          ? product[index].name
                          : product[index].name.substring(0, 28) + '...',
                      style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.BLACK),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      product[index].introduce,
                      style: AppStyle.DEFAULT_VERY_SMALL.copyWith(color: AppColor.RED),
                      maxLines: 2,
                      textAlign: TextAlign.center,
                    ),
                ]
              )
          ),
        )
    );
  }
}
