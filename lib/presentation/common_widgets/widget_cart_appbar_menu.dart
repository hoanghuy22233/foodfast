
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:foodsmy/app/constants/barrel_constants.dart';
import 'package:foodsmy/presentation/common_widgets/widget_appbar_menu.dart';

class WidgetCartAppBarMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WidgetAppbarMenu(
      backgroundColor: AppColor.PRIMARY_COLOR,
      icon: Image.asset('assets/icons/ic_cart.png', color: Colors.white,),
      onTap: () {
        AppNavigator.navigateCart();
      },
      badge: 0,
    );
  }
}
