import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodsmy/model/entity/categorys.dart';
import 'package:foodsmy/utils/color/hex_color.dart';

class Category extends StatefulWidget {
  final int id;
  Category({Key key, this.id}) : super(key: key);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child: Container(
          child: Column(
        children: [
          Image.asset(
            '${itemCategory[widget.id].image}',
            width: 45,
            height: 45,
            color: Color(
                HexColor.getColorFromHex(itemCategory[widget.id].colorImage)),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            '${itemCategory[widget.id].name}',
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold),
          ),
        ],
      )),
    );
  }
}
